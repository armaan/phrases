#!/usr/bin/env python3
# Display Latin famous phrases in the terminal
# Armaan Bhojwani 2021

import argparse
from random import randint
import sqlite3
from sys import exit
from os import path


def parse_args():
    parser = argparse.ArgumentParser(
        description="Latin famous phrases in the terminal")
    parser.add_argument("-i", "--id",
                        action='store_true',
                        help="print the id of the phrase")
    parser.add_argument("-l", "--latin",
                        action='store_true',
                        help="print the Latin phrase (default)")
    parser.add_argument("-e", "--english",
                        action='store_true',
                        help="print the English translation")
    parser.add_argument("-n", "--notes",
                        action='store_true',
                        help="print any notes on phrase")
    parser.add_argument("-v", "--version",
                        action='version',
                        version="phrases 1.0.3")
    parser.add_argument("-m", "--min",
                        default=0,
                        type=int,
                        help="set the minimum length of the Latin phrase")
    parser.add_argument("-M", "--max",
                        default=10000000,
                        type=int,
                        help="set the maximum length of Latin phrase")
    parser.add_argument("-p", "--num",
                        action='store_true',
                        help="print number of possible phrases")
    parser.add_argument("-f", "--file",
                        help="set the location of the phrase database")
    return parser.parse_args()


def output():
    data = c.fetchall()
    row = list(data[randint(0, len(data) - 1)])

    if not (args.id
            or args.latin
            or args.english
            or args.notes
            or args.num):
        print(row[1])
        exit(0)
    else:
        if args.id:
            print(row[0])
        if args.latin:
            print(row[1])
        if args.english:
            print(row[2])
        if args.notes:
            print(row[3])
        if args.num:
            print(len(data))
        exit(0)


def find_file():
    if args.file:
        return args.file
    if path.isfile("phrases.db"):
        return "phrases.db"
    elif path.isfile("/usr/local/share/phrases/phrases.db"):
        return "/usr/local/share/phrases/phrases.db"
    else:
        exit("cannot find the phrase database!")


def get_rand():
    c.execute("SELECT * FROM phrases WHERE length <= (?) AND length >= (?)",
              (args.max, args.min))


def main():
    get_rand()
    output()


if __name__ == "__main__":
    args = parse_args()
    c = sqlite3.connect(find_file()).cursor()
    main()
