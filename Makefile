.DEFAULT_GOAL := install

prep:
	mkdir -p /usr/local/bin
	mkdir -p /usr/local/man/man1

install:
	make prep
	mkdir -p /usr/local/share/phrases/
	cp phrases.py /usr/local/bin/phrases
	cp phrases.db /usr/local/share/phrases/
	cp man/phrases.1 /usr/local/man/man1/

install-extract:
	make prep
	cp extract.py /usr/local/bin/phrases-extract
	cp man/phrases-extract.1 /usr/local/man/man1/

uninstall:
	rm /usr/local/bin/phrases
	rm /usr/local/bin/phrases-extract
	rm -r /usr/local/share/phrases/
	rm /usr/local/man/man1/phrases.1
	rm /usr/local/man/man1/phrases-extract.1

reinstall:
	make uninstall
	make install
	make install-extract
