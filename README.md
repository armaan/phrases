# phrases
Latin famous phrases in the terminal

## Installation
Clone the repository to get the development version, or get the latest release from [here](https://git.sr.ht/~armaan/phrases/refs)

`sudo make install` to install the `phrases` script.

`sudo make install-extract` to install the `phrases-extract` script. This script requires the beautifulsoup4 Python module, it can be installed with `pip3 install -r requirements.txt`.

`sudo make uninstall` to uninstall completely

`sudo make reinstall` to reinstall.

The program can be run without installing as well, as it looks for the phrase database in the current working directory first.

## Source of famous phrases
Wikipedia contributors, "List of Latin phrases (full)," Wikipedia, The Free Encyclopedia, https://en.wikipedia.org/w/index.php?title=List_of_Latin_phrases_(full)&oldid=986793908. 

There are currently 2201 famous phrases in the database

## License
Phrases is MIT Licensed by [Armaan Bhojwani](https://armaanb.net), 2021. See the LICENSE file for more information.
