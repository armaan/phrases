% phrases(1) 1.0.3
% Armaan Bhojwani
% January 2021

# NAME
phrases - Latin famous phrases in the terminal

# SYNOPSIS
phrases [[options]]

# DESCRIPTION
**phrases** is a Python script that reads Latin famous phrases from a SQLite database and returns them randomly according to a set of user-defined parameters given through the CLI options. 

# OPTIONS
**-h**, **--help**
: Show a help message and exit

**-i**, **--id**
: Print the id of the phrase.

**-l**, **--latin**
: Print the Latin phrase (default)

**-e**, **--english**
: Print the English translation.

**-n**, **--notes**
: Print any notes on phrase.

**-v**, **--version**
: Print version

**-m** [length], **--min** [length]
: Set the minimum length of the Latin phrase.

**-M** [length], **--max** [length]
: Set the maximum length of Latin phrase.

**-p**, **--num**
: Print number of possible phrases.

**-f** [path], **--file** [path]
: Set the path of the phrase database.

# EXAMPLES
**phrases**
: Display a random Latin phrase

**phrases -len**
: Display a random Latin phrase, its English translation, and any notes on it

**phrases -p -m 10 -M 15**
: Display the number of phrases in the database with a length between 10 and 15 inclusive.

# EXIT VALUES
**0**
: Success

**1**
: Cannot find phrase database

**2**
: Invalid option

# SEE ALSO
  **phrases-extract(1)**

# BUGS, PATCHES
https://lists.sr.ht/~armaan/public-inbox

# COPYRIGHT
Copyright 2021 Armaan Bhojwani <me@armaanb.net>. MIT License
